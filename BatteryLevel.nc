#include "battery.h"

interface BatteryLevel {
  command uint16_t getLevel();
  event void batteryDead();
}
