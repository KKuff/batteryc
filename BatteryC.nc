#include "battery.h"

module BatteryC {
  provides interface BatteryLevel;
  uses interface BatteryUpdate;
}

implementation {

  uint16_t chargeUnits = NUM_CHARGE_UNITS;
  bool dead = FALSE;

  command uint16_t BatteryLevel.getLevel() {
    return chargeUnits;
  }

  event bool BatteryUpdate.updateEnergy(uint8_t units) {
    chargeUnits = (chargeUnits >= units) ? (chargeUnits - units) : 0;

    dbg("BatteryLevel", "Battery level is %d charge units at %s\n", chargeUnits,
      sim_time_string());

    if(chargeUnits == 0 && !dead) {
      dbg("Dead", "Node has no charge units left at %s\n%llu\n", sim_time_string(), sim_time());
      dead = TRUE;
//      signal BatteryLevel.batteryDead();
    }
    return dead;
  }
}
