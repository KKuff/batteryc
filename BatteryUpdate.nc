#include "battery.h"

interface BatteryUpdate {
  /*
   * Updates remaining battery level.
   * Returns TRUE if the battery is dead
   */
  event bool updateEnergy(uint8_t units);
}

//TossimPacketModelC provides interface BatteryUpdate, signals updateEnergy
//every time a packet is sent or received.
//
//BatteryC uses interface BatteryUpdate, implements handler for updateEnergy
//and decrements the appropriate amount
//
//BatteryC provides interface BatteryLevel, implements accessor command to get
//current battery level
//
//LinkEstimatorModP uses interface BatteryLevel, calls command getLevel in 
//order to use battery level for determining routes
